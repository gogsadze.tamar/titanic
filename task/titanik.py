import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+)\.')
    median_age_by_title = df.groupby('Title')['Age'].median().to_dict()
    def fill_age(row):
        if pd.notna(row['Age']):
            return row['Age']
        else:
            return median_age_by_title[row['Title']]


    df['Age'] = df.apply(fill_age, axis=1)
    return None
